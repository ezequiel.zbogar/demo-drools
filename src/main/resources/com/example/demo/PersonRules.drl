package com.example.demo;

import com.example.demo.entity.Person;
import com.example.demo.brm.RulesReturnValues

global com.example.demo.brm.RulesResult results;

rule "Age is in valid range"
    when
        Person(extraData.get("age") instanceof Number &&
        ((extraData.get("age") <= 0) || (extraData.get("age") > 125)))

    then
        results.addMessage("age", "La edad se encuentra en un rango invalido [0 - 125]");
        results.setReturnValue(RulesReturnValues.ERROR);
end

rule "Not contains age"
    when
        Person(!extraData.containsKey("age"))
    then
        results.addMessage("age", "El atributo es obligatorio y no fue enviado");
        results.addRequired("age");
        results.setReturnValue(RulesReturnValues.ERROR);
end

rule "Contains age"
    when
        Person(extraData.containsKey("age"))
    then
        results.addRequired("age");
end


rule "Is not older than 18"
    when
        Person(extraData.containsKey("age") && extraData.get("age") instanceof Number && extraData.get("age") < 18)
    then
        results.addRequired("parentName");
end

rule "Is older than 18"
    when
        Person(extraData.containsKey("age") && extraData.get("age") instanceof Number && extraData.get("age") >= 18)
    then
        results.addDisabled("parentName");
end

rule "Is older than 18 must contain parentName"
    when
        Person(extraData.containsKey("age") && extraData.get("age") instanceof Number && extraData.get("age") < 18 &&
         (!extraData.containsKey("parentName") || extraData.get("parentName") == null || extraData.get("parentName").toString().length() == 0))
    then
        results.addMessage("parentName", "El atributo es obligatorio y no fue enviado");
        results.setReturnValue(RulesReturnValues.ERROR);
end

rule "Contains Name"
    when
        Person(name != null && name.length() > 0)
    then
        results.addRequired("name");
end

rule "Not contains Name"
    when
        Person(name == null || name.length() == 0)
    then
        results.addMessage("name", "El atributo es obligatorio y no fue enviado");
        results.addRequired("name");
        results.setReturnValue(RulesReturnValues.ERROR);
end

rule "Contains Surname"
    when
        Person(surname != null && surname.length() > 0)
    then
        results.addRequired("surname");
end

rule "Not contains Surname"
    when
        Person(surname == null || surname.length() == 0)
    then
        results.addMessage("surname", "El atributo es obligatorio y no fue enviado");
        results.addRequired("surname");
        results.setReturnValue(RulesReturnValues.ERROR);
end

rule "Name is too long"
    when
        Person(name != null && name.length() > 10)
    then
        results.addMessage("name", "La longitud del nombre es mayor a 10 caracteres");
end
