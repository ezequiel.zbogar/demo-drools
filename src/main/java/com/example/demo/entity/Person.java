package com.example.demo.entity;

import java.util.HashMap;


public class Person implements java.io.Serializable {

    private Long id;
    private String name;
    private String surname;
    private String email;

    private HashMap<String, Object> extraData = new HashMap<String, Object>();

    public void setExtraFields(HashMap<String, Object> extraData){
        this.extraData = extraData;
    }

    public Boolean isNormalField(String field){
        return (field.equals("id") || field.equals("name")|| field.equals("surname") || field.equals("email"));
    }

    public void updateFields(Person otherPerson){
        if(otherPerson.getName() != null) this.name = otherPerson.getName();
        if(otherPerson.getSurname() != null) this.surname = otherPerson.getSurname();
        if(otherPerson.getEmail() != null) this.email = otherPerson.getEmail();

        if(otherPerson.extraData != null){
            for(String key : otherPerson.extraData.keySet()){
                this.extraData.put(key, otherPerson.extraData.get(key));
            }
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public HashMap<String, Object> getExtraData() {
        return extraData;
    }
}
