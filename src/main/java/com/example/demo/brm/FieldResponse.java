package com.example.demo.brm;

public enum FieldResponse {
    REQUIRED,
    MESSAGES,
    INVALID_TYPE,
    DISABLED
}
