package com.example.demo.brm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

public class RulesResult {

    private HashMap<FieldResponse, Object> results;
    private RulesReturnValues returnValue;

    public RulesResult() {
        this.results = new HashMap<>();
        this.returnValue = RulesReturnValues.SUCCESS;

        results.put(FieldResponse.REQUIRED, new ArrayList<String>());
        results.put(FieldResponse.DISABLED, new ArrayList<String>());
        results.put(FieldResponse.INVALID_TYPE, new ArrayList<String>());

        results.put(FieldResponse.MESSAGES, new ArrayList<HashMap<String, String>>());
    }

    public void addRequired(String field) {
        ((ArrayList) this.results.get(FieldResponse.REQUIRED) ).add(field);
    }

    public void addDisabled(String field) {
        ((ArrayList) this.results.get(FieldResponse.DISABLED) ).add(field);
    }

    public void addInvalidType(String field) {
        ((ArrayList) this.results.get(FieldResponse.INVALID_TYPE) ).add(field);
    }

    public void addMessage(String field, String msg) {
        ArrayList<HashMap<String, Object>> fieldMessages = (ArrayList<HashMap<String, Object>>) this.results.get(FieldResponse.MESSAGES);

        Optional<HashMap<String, Object>> messageObject = fieldMessages.stream().filter(item -> item.get("name").equals(field)).findAny();

        if (messageObject.isPresent()) {
            ArrayList<String> messagesList = (ArrayList<String>) messageObject.get().get("message");
            messagesList.add(msg);
        } else {
            HashMap<String, Object> itemMap = new HashMap<>();
            ArrayList<String> messagesList = new ArrayList<>();
            messagesList.add(msg);
            itemMap.put("message", messagesList);
            itemMap.put("name", field);
            fieldMessages.add(itemMap);
        }
    }

    public void setReturnValue(RulesReturnValues returnValue) {
        this.returnValue = returnValue;
    }
    
    public void setResults(HashMap<FieldResponse, Object> results) {
        this.results = results;
    }

    public RulesReturnValues getReturnValue() {
        return returnValue;
    }

    public HashMap<FieldResponse, Object> getResults() {
        return results;
    }
}
